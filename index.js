var Passport = require('passport').constructor;

module.exports = function (sails) {
  return {
    initialize: function (cb) {
      // default configuration of the tokenlife
      sails.config.oauth = { ...sails.config.oauth, tokenLife: 3600 };

      sails.after('hook:orm:loaded', function () {
        // Create instance of passport and bind it to sails.
        sails.passport = new Passport();

        return cb();
      });

    },

    configure: function () {
    },

    routes: {
      before: {
        '/*': function configurePassport(req, res, next) {
          sails.passport.initialize()(req, res, function(err) {
            if (err) return res.negotiate(err);
            sails.passport.session()(req, res, function(err) {
              if (err) return res.negotiate(err);
              next();
            });
          });
        }
      }
    }
  }
};
